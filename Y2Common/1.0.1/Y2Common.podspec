#
# Be sure to run `pod lib lint Y2Common.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Y2Common'
  s.version          = '1.0.1'
  s.summary          = 'A short description of Y2Common.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitee.com/wyky_ios/Y2Common'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'maqianzheng' => 'maqianzheng@game2sky.com' }
  s.source           = { :git => 'https://gitee.com/wyky_ios/Y2Common.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'
#  s.static_framework = true
  s.swift_version = '5.0'
  
  s.subspec 'Common' do |ss|
      ss.source_files = "Y2Common/Common/**/*"
  end
  
  s.subspec 'Encrypt' do |ss|
      ss.source_files = "Y2Common/Encrypt/*.{h,m}"
  end
  
  s.subspec 'NetWorking' do |ss|
      ss.source_files = "Y2Common/NetWorking/*.{h,m}"
      ss.dependency 'Y2NetWorking'
      ss.dependency 'Y2Common/Encrypt'
      ss.dependency 'Y2Common/Common'
  end
   
    
  # s.resource_bundles = {
  #   'Y2Common' => ['Y2Common/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
   
end
