#
#  Be sure to run `pod spec lint SDK_OpenSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.

  s.name         = "YCSuperSDK_Plugin_VipCustomer"
  s.version      = "0.2.0"
  s.summary      = "VipCustmoer Plugin for SuperSDK"

  s.homepage     = "https://gitee.com/wyky_ios/SuperSDK_Plugin_VipCustomer"

  s.license      = { :type => "Apache", :file => "LICENSE" }

  s.author             = { "tujun" => "tujun@game2sky.com" }
  
  s.platform     = :ios, "8.0"

  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the location from where the source should be retrieved.
  #  Supports git, hg, bzr, svn and HTTP.
  #

  s.source       = { :git => "https://gitee.com/wyky_ios/SuperSDK_Plugin_VipCustomer.git", :tag => "#{s.version}" }


  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  CocoaPods is smart about how it includes source code. For source files
  #  giving a folder will include any swift, h, m, mm, c & cpp files.
  #  For header files it will include any header in the folder.
  #  Not including the public_header_files will make all headers public.
  #

  s.source_files  = "src/**/*.{h,m}", "vipcustomer/**/*.{h,m}", "include/**/*.h"
  s.exclude_files = "Pods/**/*", "Doc/**/*"

#  s.public_header_files = "include/**/*.h"
  #s.private_header_files = "vipcustomer/**/*.h"
  s.header_mappings_dir = "include"


  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  A list of resources included with the Pod. These are copied into the
  #  target bundle with a build phase script. Anything else will be cleaned.
  #  You can preserve files from being cleaned, please don't preserve
  #  non-essential files like tests, examples and documentation.
  #

  s.resources = "resource/*"


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  s.frameworks = ["Security", "CFNetwork", "Foundation", "MobileCoreServices", "SystemConfiguration"]

  s.vendored_frameworks = "dlibs/*.framework"
  s.vendored_libraries = "libs/**/*.a"

  s.libraries = "c++", "z"

  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  s.requires_arc = true

  #s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(PROJECT_DIR)/include" }
  s.dependency "YCSuperSDK"
  s.dependency "HyphenateLite"
end
