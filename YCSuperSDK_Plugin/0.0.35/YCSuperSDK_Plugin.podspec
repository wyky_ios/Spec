#
#  Be sure to run `pod spec lint YCSuperSDK_Plugin.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "YCSuperSDK_Plugin"
  s.version      = "0.0.35"
  s.summary      = "Plugins for SuperSDK"

  s.homepage     = "https://gitee.com/wyky_ios/SuperSDK_Plugin.git"
  s.license      = { :type => "Apache", :file => "LICENSE" }

  s.author       = { "tujun" => "tujun@game2sky.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/wyky_ios/SuperSDK_Plugin.git", :tag => "#{s.version}", :submodules => true }

  s.exclude_files = "Pods/**/*"

  # s.public_header_files = "Classes/**/*.h"


  s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  s.dependency "YCSuperSDK"

  s.pod_target_xcconfig = {
        'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
    }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

  s.subspec 'Survey' do |ss|
    ss.source_files = 'Survey/*.{h,m}'
  end

  s.subspec 'PhotoPicker' do |ss|
     ss.source_files = 'PhotoPicker/*.{h,m}'
  end

  s.subspec 'WechatShare' do |ss|
    ss.dependency 'WeChatOpenSDK_Multi-Delegate'
    ss.source_files = 'WechatShare/*.{h,m}'
    ss.resources = "WechatShare/resource/*"
  end
  
  s.subspec 'Browser' do |ss|
    ss.source_files = 'Browser/Classes/*.{h,m}'
    ss.resources = "Browser/Assets/*"
    s.frameworks = ["Security", "CoreGraphics", "CoreTelephony", "SystemConfiguration", "WebKit"]
    s.libraries = "iconv", "sqlite3", "stdc++", "z"
    ss.vendored_frameworks = "Browser/Lib/*.framework"
    ss.dependency 'WeChatOpenSDK_Multi-Delegate'
  end
  
  s.subspec 'Captcha' do |ss|
    ss.source_files = 'Captcha/*.{h,m}'
    ss.frameworks = ["WebKit"]
  end

  s.subspec 'QrGen' do |ss|
    ss.source_files = 'QrGen/*.{h,m}'
  end

  s.subspec 'QrScan' do |ss|
    ss.source_files = 'QrScan/*.{h,m}'
  end

  s.subspec 'SDKScan' do |ss|
    ss.dependency "YCSuperSDK_Plugin/QrScan"
    ss.source_files = 'SDKScan/*.{h,m}'
  end

  s.subspec 'NotchScreen' do |ss|
    ss.source_files = 'NotchScreen/*.{h,m}'
  end

  s.subspec 'SuperPush_XinGe' do |ss|
    ss.source_files = 'SuperPush_XinGe/*.{h,m}'
    ss.vendored_libraries = "SuperPush_XinGe/libs/**/*.a"
    ss.frameworks = ["CoreTelephony", "SystemConfiguration", "UserNotifications"]
    ss.libraries = 'z','sqlite3.0'
  end
  
  s.subspec 'SuperPush_JPush' do |ss|
    ss.source_files = 'SuperPush_JPush/*.{h,m}'
    ss.dependency 'JCore', '2.4.0'
    ss.dependency 'JPush', '3.3.6'
    ss.pod_target_xcconfig = {
      'ARCHS[sdk=iphonesimulator*]' => '$(ARCHS_STANDARD_64_BIT)'
    }
  end

  s.subspec 'AppPromotion' do |ss|
    ss.source_files = 'AppPromotion/*.{h,m}'
    ss.frameworks = ["AdSupport"]
  end

  s.subspec 'ScreenBrightness' do |ss|
    ss.source_files = 'ScreenBrightness/*.{h,m}'
  end

  s.subspec 'ToolBox' do |ss|
    ss.source_files = 'ToolBox/*.{h,m}'
  end

  s.subspec 'Reyun' do |ss|
    ss.source_files = 'Reyun/*.{h,m}'
    ss.vendored_libraries = "Reyun/libs/*.a"
    ss.frameworks = ["Security", "CoreTelephony", "AdSupport", "SystemConfiguration", "CoreMotion", "CoreLocation", "iAd", "AVFoundation", "CFNetwork", "WebKit"]
    ss.libraries = 'sqlite3', 'z', 'resolv'
  end

  s.subspec 'TalkingData' do |ss|
    ss.source_files = 'TalkingData/*.{h,m}'
    ss.vendored_libraries = "TalkingData/libs/*.a"
    ss.frameworks = ["Security", "CoreTelephony", "AdSupport", "SystemConfiguration"]
    ss.libraries = 'z'
  end

  s.subspec 'ChartBoost' do |ss|
    ss.source_files = 'ChartBoost/*.{h,m}'
    ss.vendored_frameworks = "ChartBoost/libs/Chartboost.framework"
    ss.frameworks = ["StoreKit", "Foundation", "CoreGraphics", "WebKit", "AVFoundation", "UIKit"]
  end

  s.subspec 'NativeSystemCallback' do |ss|
    ss.source_files = 'NativeSystemCallback/*.{h,m}'
  end

  s.subspec 'GameController' do |ss|
    ss.frameworks = ["GameController"]
    ss.source_files = 'GameController/*.{h,m}'
  end

end
