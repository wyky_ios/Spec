#
#  Be sure to run `pod spec lint YCSuperSDK_Plugin.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "YCSuperSDK_Plugin"
  s.version      = "0.0.1"
  s.summary      = "Plugins for SuperSDK"

  s.homepage     = "https://gitee.com/wyky_ios/SuperSDK_Plugin.git"
  s.license      = { :type => "Apache", :file => "LICENSE" }

  s.author       = { "tujun" => "tujun@game2sky.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/wyky_ios/SuperSDK_Plugin.git", :tag => "#{s.version}", :submodules => true }

  s.exclude_files = "Pods/**/*"

  # s.public_header_files = "Classes/**/*.h"


  s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  s.dependency "YCSuperSDK"


  s.subspec 'Survey' do |ss|
    ss.source_files = 'Survey/*.{h,m}'
  end

  s.subspec 'PhotoPicker' do |ss|
     ss.source_files = 'PhotoPicker/*.{h,m}'
  end

  s.subspec 'WechatShare' do |ss|
    ss.dependency "WechatOpenSDK_NoPay"
    ss.source_files = 'WechatShare/*.{h,m}'
    ss.resources = "WechatShare/resource/*"
  end
end
