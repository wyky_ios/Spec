#
#  Be sure to run `pod spec lint OpenSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "YCOpenSDK"
  s.version      = "0.2.4.2"
  s.summary      = "OpenSDK For IOS"

  s.homepage     = "https://gitee.com/wyky_ios/OpenSDK.git"
  s.license      = { :type => "Apache", :file => "LICENSE" }

  s.author       = { "tujun" => "tujun@game2sky.com" }
  s.platform     = :ios, "9.0"

  s.source       = { :git => "https://gitee.com/wyky_ios/OpenSDK.git", :tag => "#{s.version}", :submodules => true }


  s.exclude_files = "Pods/**/*", "Resource/**/*", "DianLiangPay/**/*"

  # s.public_header_files = "Classes/**/*.h"

  # s.preserve_paths = "FilesToSave", "MoreFilesToSave"
  s.pod_target_xcconfig = {
        'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
    }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

  s.requires_arc = true

  s.subspec 'Framework' do |ss|
    ss.source_files = "Framework/**/*.{h,m}"
    ss.resources = "Framework/YCImage.xcassets", "Framework/storyboard/**/*", "Framework/resource/*", "Framework/Localization/**/*.Bundle"
    ss.dependency 'JCore', '2.4.0-noidfa'
    ss.dependency 'JAnalytics', '2.1.2'
  end

  s.subspec 'ApplePay' do |ss|
    ss.dependency 'YCOpenSDK/Framework'
    ss.source_files = 'ApplePay/*.{h,m}'
    ss.framework = "StoreKit"
    ss.resources = "ApplePay/*.png"
  end

  s.subspec 'AliPay' do |ss|
    ss.dependency 'YCOpenSDK/Framework'
    ss.source_files = 'AliPay/*.{h,m}'
    ss.vendored_frameworks = "AliPay/AlipaySDK.framework"
    ss.frameworks = ['SystemConfiguration','CoreGraphics','CoreTelephony','QuartzCore','CoreText','UIKit','Foundation','CFNetwork','CoreMotion']
    ss.libraries = 'c++','z'
    ss.resources = "AliPay/AlipaySDK.bundle", "AliPay/*.png"
  end

  s.subspec 'UnionPay' do |ss|
    ss.dependency 'YCOpenSDK/Framework'
    ss.source_files = 'UnionPay/*.{h,m}'
    ss.resources = "UnionPay/*.png"
  end

  s.subspec 'PayNow' do |ss|
    ss.dependency 'YCOpenSDK/Framework'
    ss.source_files = 'PayNow/*.{h,m}'
    ss.resources = "PayNow/*.png"
  end

  #Wechat Pay
  s.subspec 'WechatPay' do |ss|
    ss.dependency 'YCOpenSDK/Framework'
    ss.dependency 'WeChatOpenSDK_Multi-Delegate'
    ss.source_files = 'WechatPay/*.{h,m}'
    ss.resources = "WechatPay/*.png"
  end
  
  #Wechat Login
  s.subspec 'WechatLogin' do |ss|
    ss.dependency 'YCOpenSDK/Framework'
    ss.dependency 'WeChatOpenSDK_Multi-Delegate'
    ss.source_files = 'WechatLogin/*.{h,m}'
    ss.resources = "WechatLogin/*.png"
  end
  
  s.subspec 'QQLogin' do |ss|
    ss.dependency 'YCOpenSDK/Framework'
    ss.source_files = 'QQLogin/*.{h,m}'
    ss.vendored_frameworks = "QQLogin/*.framework"
  end

  #Apple Login
  s.subspec 'AppleLogin' do |ss|
    ss.dependency 'YCOpenSDK/Framework'
    ss.frameworks = ['UIKit', 'AuthenticationServices']
    ss.source_files = 'AppleLogin/*.{h,m}'
#    ss.resources = "AppleLogin/*.png"
  end

  s.subspec 'OneKeyLogin' do |ss|
    ss.dependency 'YCOpenSDK/Framework'
    ss.source_files = 'OneKeyLogin/*.{h,m}'
    ss.dependency 'JVerification', '2.6.7'
  end
end
