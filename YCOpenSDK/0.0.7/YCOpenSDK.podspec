#
#  Be sure to run `pod spec lint OpenSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "YCOpenSDK"
  s.version      = "0.0.7"
  s.summary      = "OpenSDK For IOS"

  s.homepage     = "https://gitee.com/wyky_ios/OpenSDK.git"
  s.license      = { :type => "Apache", :file => "LICENSE" }

  s.author             = { "tujun" => "tujun@game2sky.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitee.com/wyky_ios/OpenSDK.git", :tag => "#{s.version}", :submodules => true }


  s.exclude_files = "Pods/**/*", "Resource/**/*", "DianLiangPay/**/*"

  # s.public_header_files = "Classes/**/*.h"

  # s.preserve_paths = "FilesToSave", "MoreFilesToSave"


  s.requires_arc = true

  s.subspec 'Framework' do |ss|
    ss.dependency 'OpenSSL', '~> 1.0.210'

    ss.source_files = "Framework/**/*.{h,m}"
    ss.resource = "Framework/resource/*"
  end

  s.subspec 'ApplePay' do |ss|
    ss.dependency 'YCOpenSDK/Framework'

    ss.source_files = 'ApplePay/*.{h,m}'
    ss.framework = "StoreKit"
  end

  s.subspec 'AibeiPay' do |ss|
    ss.dependency 'YCOpenSDK/Framework'

    ss.source_files = 'AibeiPay/*.{h,m}'
    ss.vendored_frameworks = ["AibeiPay/RSAKit.framework"]
  end


  s.subspec 'AliPay' do |ss|
    ss.dependency 'YCOpenSDK/Framework'

    ss.source_files = 'AliPay/*.{h,m}'
    ss.vendored_frameworks = "AliPay/AlipaySDK.framework"
ss.frameworks = 'SystemConfiguration','CoreGraphics','CoreTelephony','QuartzCore','CoreText','UIKit','Foundation','CFNetwork','CoreMotion'
    ss.libraries = 'c++','z'
    ss.resources = "AliPay/AlipaySDK.bundle"
  end

  s.subspec 'UnionPay' do |ss|
    ss.dependency 'YCOpenSDK/Framework'

    ss.source_files = 'UnionPay/*.{h,m}'
  end

  s.subspec 'PayNow' do |ss|
    ss.dependency 'YCOpenSDK/Framework'

    ss.source_files = 'PayNow/*.{h,m}'
  end

end
